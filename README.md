# Communications_Training

Begun during DebConf20-ONLINE.  https://debconf20.debconf.org/talks/66-how-to-organize-peer-to-peer-communications-training/

Seeking collaborators for development of Debian communications training curriculum hand-book or books.  

This curriculum-design project is primarily developed for trainers, people helping others learn.  So for example, we will focus primarily on "*how to teach* best practices in Debian communications".  Tho of course we include "*how to communicate* well in Debian".  Our results will hopefully  be helpful to community deliberations in general, while having a primary focus on Debian's community needs.

Our wiki is the project's primary text, where future documents are developed.  https://salsa.debian.org/Delib/communications_training/-/wikis/home 
